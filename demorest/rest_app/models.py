from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your models here.
class WikiHref(models.Model):
    name = models.CharField(verbose_name='Name', db_index=True, unique=True,max_length=64)
    urls = models.CharField(verbose_name="URL", max_length=100)
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)