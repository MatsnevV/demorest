from rest_framework import serializers
from .models import WikiHref

class WikiListSerializer(serializers.ModelSerializer):
    class Meta:
        model = WikiHref
        fields = '__all__'