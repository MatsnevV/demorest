from django.shortcuts import render
from rest_framework import generics
from .serializers import WikiListSerializer
from .models import WikiHref

class WikiCreateView(generics.CreateAPIView):
    serializer_class = WikiListSerializer

class WikiListView(generics.ListAPIView):
    serializer_class = WikiListSerializer
    queryset = WikiHref.objects.all()

class WikiDetalView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WikiListSerializer
    queryset = WikiHref.objects.all()
# Create your views here.
