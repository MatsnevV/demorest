from django.contrib import admin
from django.urls import path
from .views import *

app_name = 'wikihref'
urlpatterns = [
    path('wiki/create/', WikiCreateView.as_view()),
    path('wiki/all/', WikiListView.as_view()),
    path('wiki/detail/<int:pk>/', WikiDetalView.as_view()),
]